import React from 'react';
import Home from './components/home'
import Navbar from './components/navbar'
import Footer from './components/footer'
import Scrollbutton from './components/Scrollbutton'
import Map from './components/Map'

import {Route,Switch,BrowserRouter} from 'react-router-dom';

function App() {
  return (
    <div className="App">
    
    <BrowserRouter>
      <Navbar/>
        <Switch>
          <Route
            exact path= '/'
            component = {Home} 
          />

            <Route
            exact path= '/maps'
            component = {Map} 
          />
          
        </Switch>
        <Scrollbutton/>
        
    </BrowserRouter>
    <Footer/>
    
    </div>
  );
}

export default App;
